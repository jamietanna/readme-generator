module gitlab.com/jamietanna/readme-generator

go 1.17

require willnorris.com/go/microformats v1.1.1 // direct

require golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa // indirect
