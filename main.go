package main

import (
	"embed"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"sync"
	"text/template"

	"willnorris.com/go/microformats"
)

type readmeData struct {
	Hcard          *microformats.Microformat
	HcardJob       string
	Articles       []*microformats.Microformat
	Blogumentation []*microformats.Microformat
	Bookmarks      []*bookmark
	WeekNote       map[string][]interface{}
	LastRead       string
	LastSteps      string
}

type bookmark struct {
	Title string
	URL   string
}

func hcardJob(hcard *microformats.Microformat) string {
	if _, ok := hcard.Properties["job-title"]; !ok {
		return "I'm currently between jobs"
	}
	var job = "I'm currently a "
	job += hcard.Properties["job-title"][0].(string)
	job += " at "

	var org = hcard.Properties["org"][0].(*microformats.Microformat)
	job += "[" + org.Properties["name"][0].(string) + "](" + org.Properties["url"][0].(string) + ")"

	return job
}

func mf2(mf2Url string) *microformats.Data {
	u, _ := url.Parse(strings.TrimSpace(mf2Url))
	resp, err := http.Get(u.String())
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	return microformats.Parse(resp.Body, u)
}

func lastRead() string {
	var data = mf2("https://www.jvt.me/kind/reads/")
	var readOf = data.Items[0].Children[0].Properties["read-of"][0].(*microformats.Microformat)

	var authors = readOf.Properties["author"]

	var sb strings.Builder
	sb.WriteString("_" + readOf.Properties["name"][0].(string) + "_ by ")

	for i, author := range authors {
		switch v := author.(type) {
		case *microformats.Microformat:
			sb.WriteString(v.Value)
			if i+1 != len(authors) {
				sb.WriteString(" and ")
			}

		case string:
			sb.WriteString(v)
		}
	}

	return sb.String()
}

func lastSteps() string {
	var data = mf2("https://www.jvt.me/kind/steps/")
	var steps = data.Items[0].Children[0].Properties

	return steps["num"][0].(string) + " " + steps["unit"][0].(string)
}

func lastWeekNote() map[string][]interface{} {
	var data = mf2("https://www.jvt.me/week-notes/")
	return data.Items[0].Children[0].Properties
}

func getNumberOfMicroformats(url string, count int) []*microformats.Microformat {
	var data = mf2(url)
	return data.Items[0].Children[0:count]
}

func articles() []*microformats.Microformat {
	return getNumberOfMicroformats("https://www.jvt.me/kind/articles/", 3)
}

func blogumentation() []*microformats.Microformat {
	return getNumberOfMicroformats("https://www.jvt.me/tags/blogumentation/", 5)
}

func bookmarks() []*bookmark {
	mf2 := getNumberOfMicroformats("https://www.jvt.me/kind/bookmarks/", 5)

	bookmarks := make([]*bookmark, 5)
	for i, theBookmark := range mf2 {
		var url string
		var name string = ""
		if len(theBookmark.Properties["name"]) > 0 {
			name = theBookmark.Properties["name"][0].(string)
		}

		switch v := theBookmark.Properties["bookmark-of"][0].(type) {
		case *microformats.Microformat:
			if len(v.Properties["name"]) > 0 {
				name = v.Properties["name"][0].(string)
			}
			url = v.Value
		case string:
			url = v
		}

		if name == "" {
			name = url
		}

		bookmarks[i] = &bookmark{
			Title: name,
			URL:   url,
		}
	}

	return bookmarks
}

func addTracking(theURL string) string {
	u, err := url.Parse(theURL)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return theURL
	}

	queryString := u.Query()

	campaign := ""
	switch os.Getenv("README_HOST") {
	case "gitlab":
		campaign += "gitlab-jamietanna"
	case "github":
		campaign += "github-jamietanna"
	case "readme.jvt.me":
		campaign += "readme.jvt.me"
	}

	if len(campaign) == 0 {
		return theURL
	}

	queryString.Set("utm_campaign", campaign)
	u.RawQuery = queryString.Encode()

	return u.String()
}

//go:embed templates
var templates embed.FS

func main() {
	var readmeData readmeData
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		homepage := mf2("https://www.jvt.me")
		readmeData.Hcard = homepage.Items[0].Children[0]
		readmeData.HcardJob = hcardJob(readmeData.Hcard)
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		readmeData.Articles = articles()
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		readmeData.Blogumentation = blogumentation()
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		readmeData.Bookmarks = bookmarks()
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		readmeData.WeekNote = lastWeekNote()
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		readmeData.LastRead = lastRead()
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		readmeData.LastSteps = lastSteps()
		wg.Done()
	}()

	wg.Wait()

	readmeTemplate, err := template.New("").Funcs(template.FuncMap{
		"AddTracking": addTracking,
	}).ParseFS(templates, "templates/README.md.tmpl")
	if err != nil {
		fmt.Fprintln(os.Stderr, fmt.Errorf("error rendering README.md template: %w", err))
		os.Exit(1)
	}

	err = readmeTemplate.ExecuteTemplate(os.Stdout, "README.md.tmpl", readmeData)
	if err != nil {
		fmt.Fprintln(os.Stderr, fmt.Errorf("error rendering README.md template: %w", err))
		os.Exit(1)
	}

	switch os.Getenv("README_HOST") {
	case "gitlab":
		fmt.Println("\n---\nThis is an [autogenerated README](" + addTracking("https://www.jvt.me/posts/2022/01/12/autogenerated-profile-readme/") + "), which is [automagically deployed using GitLab CI](https://gitlab.com/jamietanna/jamietanna/-/blob/main/.gitlab-ci.yml).")
	case "github":
		fmt.Println("\n---\nThis is an [autogenerated README](" + addTracking("https://www.jvt.me/posts/2022/01/12/autogenerated-profile-readme/") + "), which is [automagically deployed using GitHub Actions](https://github.com/jamietanna/jamietanna/blob/main/.github/workflows/rebuild.yml).")
	case "readme.jvt.me":
		fmt.Println("\n---\nThis is an [autogenerated README](" + addTracking("https://www.jvt.me/posts/2022/01/12/autogenerated-profile-readme/") + "), which is [automagically deployed using GitLab CI](https://gitlab.com/jamietanna/jamietanna/-/blob/main/.gitlab-ci.yml).")
	}
}
