# README Generator

An automated tool to generate the README for i.e. [GitLab](gitlab.com/jamietanna/jamietanna/), using [Microformats2](https://microformats.io/) metadata on [my site](https://www.jvt.me/).
